# Local Dev Env Setup

_tbd_


## References

* [Developing Alexa Skills Locally with Node.js: Setting Up Your Local Environment](https://www.bignerdranch.com/blog/developing-alexa-skills-locally-with-nodejs-setting-up-your-local-environment/)
* [Big Nerd Ranch Series: Developing Alexa Skills Locally with Node.js: Implementing Persistence in an Alexa Skill (part 4 of 6)](https://developer.amazon.com/blogs/post/Tx1T3KH2O7K8AOP/Big-Nerd-Ranch-Series-Developing-Alexa-Skills-Locally-with-Node-js-Implementing)


